resource "aws_iam_role_policy" "lambda_policy" {
  name = "lambda_policy"
  role = "${aws_iam_role.lambda_role.id}"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = "${file("iam/lambda-policy.json")}"
}

resource "aws_iam_role" "lambda_role" {
  name = "test_role"
  assume_role_policy = "${file("iam/lambda-assume-policy.json")}"
}

resource "aws_iam_role" "lambda_iam" {
  name = "trigger_lambda_role"

  assume_role_policy = "${file("iam/lambda-assume-policy.json")}"
}

resource "aws_iam_role_policy" "revoke_keys_role_policy" {
  name = "revoke_keys_policy"
  role = aws_iam_role.lambda_iam.id

  policy = "${file("iam/revoke-keys-lambda-policy.json")}"

}