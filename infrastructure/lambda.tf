locals {
  lambda_zip_location = "outputs/lambda.zip"
}

data "archive_file" "lambda" {
  type        = "zip"
  source_file = "../lambdas/lambda.py"
  output_path = "${local.lambda_zip_location}"
}

resource "aws_lambda_function" "example" {
  function_name = "ServerlessExample4"
  filename = "${local.lambda_zip_location}"
  handler = "lambda.handler"
  runtime = "python3.7"
  role = "${aws_iam_role.lambda_role.arn}"
}

