locals {
  trigger_lambda_zip_location = "outputs/triggerLambda.zip"
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.trigger-lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket.arn
}

data "archive_file" "triggerLambdaArchive" {
  type        = "zip"
  source_file = "../lambdas/triggerLambda.py"
  output_path = "${local.trigger_lambda_zip_location}"
}

# Creating Lambda resource
resource "aws_lambda_function" "trigger-lambda" {
  function_name = "triggerLambda"
  filename = "${local.trigger_lambda_zip_location}"
  role = "${aws_iam_role.lambda_iam.arn}"
  handler = "triggerLambdaArchive.handler"
  runtime = "python3.7"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "trigger-lambda-test-bucket-1"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.trigger-lambda.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}