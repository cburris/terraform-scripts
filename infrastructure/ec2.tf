locals {
  account_id = data.aws_caller_identity.current.account_id
}

data "aws_ami" "centos-7" {
  owners      = ["679593333241"]
  most_recent = true
  filter {
      name = "name"
      values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }
  filter {
      name = "architecture"
      values = ["x86_64"]
  }
  filter {
      name = "root-device-type"
      values = ["ebs"]
  }
}

# resource "aws_network_interface" "network" {
#   subnet_id   = "${aws_subnet.my_subnet.id}"
#   vpc_security_group_ids = [vpc_security_group_ids]
# }

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.centos-7.id}"
  instance_type = "t3.nano"

  # network_interface {
  #   network_interface_id = "${aws_network_interface.network.id}"
  #   device_index         = 0
  # }  
}